# 3088 PiHat Project

Our PiHat is a gas leak detector that has a built in backup power supply. The purpose of the hat is for it to be used in any situation where their is potential for a gas leak to occur so that the hat can audibly and visually warn anyone in the area that there is a potentially hazardous gas leak in process.

How to use the PiHat:

In order to use the PiHat as a gas detector as intended it should be placed in the room/building where a gas leak might be detected and the program associated with the hat should be run on the Pi-Zero.

Bill of materials:

1x 5K resistor
1x 1.8K resistor
1x 100uF capacitor
1x 1mF capacitor
3x 1N5822 diodes
1x 100uH inductor
1x miro-USB connector
1x LM2576 voltage regulator

(optional) 1x circuit board

